**Thématique** :Algorithme et programmation 

**Notions liées** :Instruction, affectation, boucles, structures conditionnelles, fonction 

**Résumé de l’activité** :les élèves sont invités à donner des instructions dans l'interpréteur python.

**Objectifs** :Maîtriser le concept des instructions.

**Auteur** :Lagbouri Younes

**Durée de l’activité** :une heure (après la prise de connaissance du cours).

**Forme de participation** :binôme, sur machine.

**Matériel nécessaire** :ordinateur avec  python installé

**Préparation** :préparation du scénario pédagogique

**Fiche élève cours** :Disponible [ici](https://mega.nz/file/xNBwjJxK#gvIpv9HCs_3XpYsCbP4S-ixkXVi3duG5yVAw8qCcFC4)

**Fiche élève activité** :Disponible [ici](https://mega.nz/file/1YZThLoR#fVusje7UeK8LN3eBSiTvIf2CdBRGgY5kGvRSGz0mzA4)
